
import './App.css';
import NavbarComponent from './component/NavbarComponent';
import HomePage from './page/HomePage';
import { Route, Routes } from 'react-router-dom';
import CreatePostPage from './page/CreatePostPage';
import EditArticle from './page/EditArticle';
import ViewPage from './page/ViewPage';
import CategoryPage from './page/CategoryPage';
import CreateCategory from './page/CreateCategory';
import UpdateCategory from './page/UpdateCategory';
import ViewCategory from './page/ViewCategory';

function App() {
  return (
    <div className="App">
      <NavbarComponent/>
        <Routes>
          <Route path='/' element={<HomePage/>}/>
          <Route path='/add' element={<CreatePostPage/>}/>
          <Route path='/category' element={<CategoryPage/>}/>
          <Route path='/edit' element={<EditArticle/>}/>
          <Route path='/view' element={<ViewPage/>}/>
          <Route path='/createCategory' element={<CreateCategory/>}/>
          <Route path='/category/edit' element={<UpdateCategory/>}/>
          <Route path='/category/view' element={<ViewCategory/>}/>
        </Routes>
    </div>
  );
}

export default App;
