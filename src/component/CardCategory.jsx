import React from 'react'
import { Button, Card } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

export default function CardCategory({ item, handleDelete, handleEdit }) {

    const nav = useNavigate()

    handleEdit = () => {
        nav("/category/edit", { state: { ...item } })
    }
    const handleView = () => {
        nav("/category/view", { state: { ...item } });
    }

    return (
        <div className="mb-2" style={{ display: 'flex' }}>

            <Card border="success" className="my-2 " style={{ borderRadius: "20px", width: '13rem' }}>
                <Card.Body>
                    <Card.Title style={{ height: "50px", textAlign: 'center' }}>{item.name}</Card.Title>
                    <div className="d-flex flex-column mb-2">
                        <Button variant="primary" className="mb-2"
                            onClick={() => handleEdit(item._id)}><b>Edit</b></Button>
                        <Button variant="warning" className="mb-2"
                            onClick={handleView}><b>View</b></Button>
                        <Button variant="danger" className="mb-2"
                            onClick={() => handleDelete(item._id)}><b>Delete</b></Button>
                    </div>
                </Card.Body>

            </Card>


        </div>
    )
}
