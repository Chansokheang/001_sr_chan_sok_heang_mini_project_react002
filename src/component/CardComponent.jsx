import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CardComponent({ item, handleDelete, handleEdit }) {

    const nav = useNavigate()

    handleEdit = () => {
        nav("/edit", { state: { ...item } })
    }
    const handleView = () => {
        nav("/view", { state: { ...item } });
    }
    return (
        <div style={{ display: 'flex' }}>
            <Card border="success" className="my-2" style={{ borderRadius: '10px',width: '100%', height: '35rem' }}>
                <Card.Img className="mb-2 p-2"
                    style={{ height: '200px', borderRadius: '25px', objectFit: 'cover' }} variant="top"
                    src={item.image ?? "https://t4.ftcdn.net/jpg/04/70/29/97/360_F_470299797_UD0eoVMMSUbHCcNJCdv2t8B2g1GVqYgs.jpg"} />
                <Card.Body >
                    <Card.Title className="txt" style={{ height: '50px', textAlign: 'center' }}>
                        {item.title}
                    </Card.Title>
                    <Card.Text className="txt" style={{ height: '100px', textAlign: 'center' }} >
                        {item.description ?? "No info"}
                    </Card.Text>
                    <div className="d-flex flex-column mb-2">
                        <Button variant="primary"
                            onClick={() => handleEdit(item._id)} className="mb-2"><b>Edit</b></Button>
                        <Button variant="warning"
                            onClick={handleView} className="mb-2"><b>View</b></Button>
                        <Button variant="danger"
                            onClick={() => handleDelete(item._id)} className="mb-2" F><b>Delete</b></Button>
                    </div>
                </Card.Body>
            </Card>
        </div>
    );
}
