import React, { useEffect, useState } from 'react'
import CardComponent from '../component/CardComponent'
import { api } from "../api/api";
import { Button, Col, Container, Nav, Row } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import CardCategory from '../component/CardCategory';

export default function CategoryPage() {

  const [data, setData] = useState([])
  useEffect(() => {
      api.get("/category").then((r) => {
          setData(r.data.payload);
      })
  }, []);

  const handleDelete = (id) => {
      const newData = data.filter(data => data._id !== id)
      setData(newData)
      api.delete(`/category/${id}`).then((r) => { console.log(r.data.message); })
  }
  const handleEdit = (id) =>{
      const newData = data.filter(data => data._id !== id)
      setData(newData)
      api.delete(`/category/${id}`).then((r) => { console.log(r.data.message); })
  }
  return(
    <div>
    <Container>
        <div style={{ margin: '50px 0' }} >
            <Row>
                <Col>
                    <div>
                        <h1>All Category</h1>
                    </div>
                </Col>
                <Col>
                    <div style={{ textAlign: 'end' }}>
                        <Button variant="outline-dark"><Nav.Link as={NavLink} to='/createCategory'><b>New Category</b></Nav.Link></Button>
                    </div>
                </Col>
            </Row>
        </div>

        <Row>
            {data.map((item, index) => (
                <Col xs={12} sm={6} md={4} lg={2} key={index}>
                    <CardCategory item={item} handleDelete={handleDelete} handleEdit={handleEdit} />
                </Col>
            ))}
        </Row>
    </Container>
</div>
  )
}
