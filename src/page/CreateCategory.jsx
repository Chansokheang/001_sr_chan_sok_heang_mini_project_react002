import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import { api } from '../api/api'

export default function CreateCategory() {
  const [name, setName] = useState();

  const nav = useNavigate()

  const handleCancel = () => {
    nav("/")
  }

  const handleSubmit = () => {

    Swal.fire({
      title: 'Do you want to save the changes?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Save',
      denyButtonText: `Don't save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        api
        .post("/category", { name })
        .then((res) => console.log(res.data.message))
        .then(Swal.fire('Saved!', '', 'success'))
        nav("/category")
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
        nav("/category")
      }
    })


  }
  const handleNameChange = (e) => {
    setName(e.target.value)
  }
  return (
    <Container>
      <Form>
        <h1 style={{ marginTop: '50px' }}>Add New Article</h1>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label><b>Title</b></Form.Label>
          <Form.Control
            type="text"
            placeholder="Name"
            onChange={handleNameChange}
          />
        </Form.Group>
      </Form>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button variant="danger" onClick={handleCancel} style={{ marginRight: '5px' }}>
          Cancel
        </Button>
        <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </div>
    </Container>
  )
}
