import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import { api } from '../api/api'

export default function EditArticle({ item }) {


    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [isPublished, setIsPublished] = useState(true)
    const [image, setImage] = useState("")
    const [imageUrl, setImageUrl] = useState();
    const [ID, setID] = useState()
    let location = useLocation();
    const data = location.state
    const nav = useNavigate()

    const handleCancel = () => {
        nav("/")
    }
    const handleSubmit = () => {

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Edit',
            denyButtonText: `Don't edit`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                api
                .patch(`/articles/${ID}`, { title, description, isPublished, image })
                .then((res) => console.log(res.data.message))
                .then(Swal.fire('Saved!', '', 'success'))
                nav("/")
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    }

    useEffect(() => {
        setID(data._id)
        setTitle(data.title)
        setDescription(data.description)
        setIsPublished(data.isPublished)
        setImage(data.image)
    }, [location])

    const handleTitleChange = (e) => {
        setTitle(e.target.value)
    }
    useEffect(() => {
        console.log(title);
    }, [title])
    const handleDesChange = (e) => {
        setDescription(e.target.value);
    }
    const isPublishedChange = (e) => {
        setIsPublished(e.target.checked);
    }
    const handleImageChange = (e) => {
        setImageUrl(URL.createObjectURL(e.target.files[0]));
        const formData = new FormData();
        formData.append("image", e.target.files[0]);
        console.log("FormData : ", formData.get("image"));
        api.post("/images", formData).then((res) => setImage(res.data.payload.url));
    };
    return (
        <Container className="w-50">
            <Form>
                <h1 style={{ marginTop: '50px' }}>Edit Article</h1>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label><b>Title</b></Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="title"
                        value={title}
                        onChange={handleTitleChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label><b>Description</b></Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="description"
                        value={description}
                        onChange={handleDesChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check
                        type="checkbox"
                        label="Is Published"
                        onChange={isPublishedChange}
                    />
                    <img src={imageUrl ?? "https://t4.ftcdn.net/jpg/04/70/29/97/360_F_470299797_UD0eoVMMSUbHCcNJCdv2t8B2g1GVqYgs.jpg"} style={{ height: "200px" }} />
                    <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label><b>Image File</b></Form.Label>
                        <Form.Control type="file" onChange={handleImageChange} />
                    </Form.Group>


                </Form.Group>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button variant="danger" onClick={handleCancel} style={{ marginRight: '5px' }}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={handleSubmit}>
                        Submit
                    </Button>
                </div>
            </Form>
        </Container>
    )
}
