import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import { api } from '../api/api'

export default function UpdateCategory({ item }) {
  const [name, setName] = useState()
  const [ID, setID] = useState()
  let location = useLocation();
  const data = location.state
  const nav = useNavigate()

  const handleCancel = () => {
    nav("/")
  }
  const handleSubmit = () => {

    Swal.fire({
      title: 'Do you want to save the changes?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Edit',
      denyButtonText: `Don't edit`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        api
        .put(`/category/${ID}`, { name })
        .then((res) => console.log(res.data.message))
        nav("/category")
        Swal.fire('Saved!', '', 'success')
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })

  }
  useEffect(() => {
    setID(data._id)
    setName(data.name)
  }, [location])
  const handleNameChange = (e) => {
    setName(e.target.value)
  }
  return (
    <Container className="w-50">
      <Form>
        <h1 style={{ marginTop: '50px' }}>Edit Category</h1>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label><b>Title</b></Form.Label>
          <Form.Control
            type="text"
            placeholder="Name"
            value={name}
            onChange={handleNameChange}
          />
        </Form.Group>
      </Form>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button variant="danger" onClick={handleCancel} style={{ marginRight: '5px' }}>
          Cancel
        </Button>
        <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </div>
    </Container>
  )
}
