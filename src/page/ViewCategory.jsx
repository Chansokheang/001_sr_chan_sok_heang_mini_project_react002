import React from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'

export default function ViewCategory() {
  const location = useLocation()
  const data = location.state
  const nav = useNavigate()
  return (
    <Container>
      <div style={{ margin: '100px 0', height: '200px', padding: '50px', width: '100%' }}

        className="shadow p-3 mb-5 bg-white rounded">

        <Button variant="light" onClick={() => nav("/category")}
          className="m-3 btn-light fw-bold mb-4">
          Back
        </Button>
        <Row >
          <Col lg={12}>
            <Card style={{ border: 'none' }}>
              <Card.Body>
                <Card.Title className='m-2 txt' style={{ height: '10%' }}>
                  <h4>Name: {data.name}</h4>
                </Card.Title>
              </Card.Body>
            </Card>

          </Col>
        </Row>
      </div>
    </Container>
  )
}
