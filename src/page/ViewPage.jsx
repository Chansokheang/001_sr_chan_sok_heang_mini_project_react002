
import React from 'react'
import { Button, Card, Col, Container, Image, Row } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'

export default function ViewPage() {
    const location = useLocation()
    const data = location.state
    const nav = useNavigate()


    return (

        <Container >

            <div style={{ margin: '100px 0', height: '550px', padding: '50px', width: '100%' }}

                className="shadow p-3 mb-5 bg-white rounded">

                <Button variant="light" onClick={() => nav("/")}
                    className="m-3 btn-light fw-bold mb-4">
                    Back
                </Button>
                <Row >

                    <Col lg={6}>
                        <Image
                            src={data.image ?? "https://t4.ftcdn.net/jpg/04/70/29/97/360_F_470299797_UD0eoVMMSUbHCcNJCdv2t8B2g1GVqYgs.jpg"}
                            width="600px"
                            height="400px"
                            className='p-3'
                        // style={{ flex: 1 }}
                        />
                    </Col>
                    <Col lg={6}>
                        <Card style={{ border: 'none' }}>
                            <Card.Body>
                                <Card.Title className='m-2' style={{ height: '10%' }}>
                                    <h4>Title: {data.title}</h4>
                                </Card.Title>
                                <Card.Text className='m-2' style={{ height: '250px' }}>
                                    <h5>Description: {data.description ?? "No info"}</h5>
                                </Card.Text>
                                <Card.Text className='m-2' style={{ height: '40%' }}>
                                    <h5>Publish: {data.isPublished ? 'true': 'false'}</h5>
                                </Card.Text>
                            </Card.Body>
                        </Card>

                    </Col>
                </Row>
            </div>
        </Container>
    )
}
